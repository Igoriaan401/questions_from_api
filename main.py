from flask import Flask, jsonify, request
import requests
import psycopg2
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Column, BigInteger, String, DateTime
from sqlalchemy.exc import IntegrityError
from decouple import config


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = config('SECRET_KEY', default='')

db = SQLAlchemy(app)


class Questions(db.Model):
    __tablename__ = 'questions'
    id = Column(BigInteger, primary_key=True)
    questiontext = Column(String, unique=True)
    responsetext = Column(String)
    create_data = Column(DateTime)


def get_data(count):
    url = f"https://jservice.io/api/random?count={count}"
    response = requests.get(url)

    if response.status_code == 200:
        data = response.json()
        return data
    else:
        return None


@app.route("/questions", methods=['POST'])
def questions():
    content = request.json
    questions_num = content.get("questions_num")
    if isinstance(questions_num, int) and questions_num > 0:
        while questions_num != 0:
            questions_data = get_data(questions_num)
            for i in questions_data:
                q_id = i['id']
                q_question = i['question']
                q_answer = i['answer']
                q_created_at = i['created_at']
                new_question = Questions(id=q_id, questiontext=q_question, responsetext=q_answer, create_data=q_created_at)
                try:
                    db.session.add(new_question)
                    db.session.commit()
                    questions_num -= 1
                except IntegrityError:
                    db.session.rollback()
        return jsonify("new questions have been added")
    else:
        return jsonify("questions_num must be int and > 0")


if __name__ == '__main__':
    with app.app_context():
        db.session.connection()
    app.run()
